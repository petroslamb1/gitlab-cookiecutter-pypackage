{% set is_open_source = cookiecutter.open_source_license != 'Not open
source' -%} 
# {{ cookiecutter.project_name }} 

{{ cookiecutter.project_short_description }}

{% if is_open_source %} * Free software: {{
cookiecutter.open_source_license }} * Documentation: {{ cookiecutter.gitlab_url }}
{% endif %}
## Install

After creating your conda or pip environment:

```shell
pip install {{ cookiecutter.project_slug }}
```

## Features

-   TODO

## Develop

Install development requirements:
```shell
pip install -r requirments-dev.txt
```

Alternatively you can use `make` for all below sections:

```
clean                clean build, *.pyc and test artifacts
lint                 lint with flake8
test                 run tests with pytest
test-all             run tests on every Python version with tox
coverage             run tests with coverage
docs                 build docs with mkdocs
servedocs            build and serve docs
release              bumpversion with commit and tag
gitlab               package and upload a release
dist                 builds source and wheel package
install              make package installation
install-editable     make editable install of package
```

### Tests

Run tests with:

```shell
pytest
```

### Mkdocs

Generate docs:

```shell
mkdocs build
```

Serve them locally:

```shell
mkdocs serve
```

### Release

Bumpversion will create a new commit and tag:

```shell
bump2version patch
```

Build:

```shell
python -m build
```

Upload package to gitlab:

```shell
export TWINE_REPO_URL=https://gitlab.com/api/v4/projects/{{ cookiecutter.gitlab_project_id }}/packages/pypi
twine upload --verbose --repository-url $TWINE_REPO_URL dist/*
```
# Credits

This package was created with
[Cookiecutter](https://github.com/audreyr/cookiecutter) and the
[Gitlab Cookiecutter Pypackage](https://gitlab.com/petroslamb1/gitlab-cookiecutter-pypackage)
project template.
