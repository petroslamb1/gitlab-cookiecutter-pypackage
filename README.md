# Cookiecutter PyPackage


-   GitLab repo:
    <https://gitlab.com/petroslamb1/gitlab-cookiecutter-pypackage>
-   Free software: BSD license

This is a cookiecutter template for a python package. It contains features that are go beyond
a basic python package, but is intended to be easy to use without having to invest a lot
of time for understanding it. The automatic features of this template targeted towards 
the Gitlab servers  and should work on any GitLab server with [Gitlab-CI](https://docs.gitlab.com/ce/ci/) 
and [Gitlab pages](https://docs.gitlab.com/ce/user/project/pages/).


Adjusted from:
- https://git.geomar.de/open-source/toolbox/cookiecutter-pypackage
Which in turn was adjusted from:
- https://github.com/audreyfeldroy/cookiecutter-pypackage

## Features

-   Testing setup with `unittest` and `python setup.py test` or `pytest`
-   Issue templates
-   [Gitlab-CI](https://docs.gitlab.com/ce/ci/): Ready for Gitlab Continuous
    Integration testing
-   [Tox](http://testrun.org/tox/) testing: Setup to easily test for
    Python 3.6, 3.7, 3.8
-   [Mkdocs](http://mkdocs.org/) docs: Documentation ready for
    generation with, for example, [Gitlab pages](https://docs.gitlab.com/ce/user/project/pages/)
-   [bump2version](https://github.com/c4urself/bump2version):
    Pre-configured version bumping with a single command
-   Auto-release to [Gitlab PyPI repository](https://docs.gitlab.com/ce/user/packages/pypi_repository/) when you push a
    new tag to master (optional)
-   Command line interface using Click (optional)

## Quickstart

Save a gitlab-url, e.g. by creating a new Gitlab repo 

Install the latest Cookiecutter if you haven\'t installed it yet (this
requires Cookiecutter 1.4.0 or higher):

    pip install -U cookiecutter

Generate a Python package project:

    cookiecutter https://gitlab.com/petroslamb1/gitlab-cookiecutter-pypackage

Then:

-   Create a repo or link an existing empty repo (see instructions in GitLab)
-   Install the dev requirements into a virtualenv.
    (`pip install -r requirements_dev.txt`)
-   (Optional) [Enable shared runner](https://docs.gitlab.com/ce/ci/runners/#enable-shared-runners) 
    for your repo
-   (Optional) Activate gitlab pages for your repo
-   Release your package by pushing a new tag to master.
-   Add a `requirements.txt` file that specifies the packages you will
    need for your project and their versions. For more info see the [pip
    docs for requirements
    files](https://pip.pypa.io/en/stable/user_guide/#requirements-files).


## Implementation of features
In this paragraph, the main features and associated files are introduced. Feel free to skip over
features that you are not interested in. On the other hand, if you would like to implement some 
features in your existing project, read on to learn how they are implemented here.

Ther are many ways to structure a python project. This is only one of them, and may not
be the right way for you. This template strives for a balance between feature rich and
easy, well established and innovative. 

There is no definite way of doing stuff when it comes
to python packaging, so this cookiecutter is almost guaranteed to change over time.

### build system
A lot is happening currently when it comes to building python packages. For ages, 
[`setup.py`](https://docs.python.org/3/distutils/setupscript.html) and setuptools 
was de-facto standard. Nowadays, there is an increasing trend towards universal, 
feature-rich and automated build systems using 
[`pyproject.toml`](https://snarky.ca/what-the-heck-is-pyproject-toml/)
[Setuptools](https://setuptools.readthedocs.io/en/latest/setuptools.html) 
is still a thing, although `setup.cfg` is slowly replacing `setup.py`. Alternatives
to setuptools, such as [Poetry](https://python-poetry.org/) 
or [Flit](https://flit.readthedocs.io/en/latest/) are becomming more popular.

This template is using a retro-modern approach:

- most of the metadata and configuration is done classically in `setup.py`
- `setup.cfg` was introduced and contains additional configs for some tools
- `pyproject.toml` is present to future-proof the template, declaring 
   setuptools with wheel as the build-system of choice

The actual build is done using build tool:

```
pip install build
python -m build
```

### Testing
Pytest is configured to look for tests in the `tests` subdirectory. Look at the examples there
for a start. You should test as much of your code as you can as often as you can using pytest.
Run pytest from your IDE or from the command line.


Tox is also configured for additional testing of various python versions.

### Docs

Static html pages are generated from your README.md and your source code using 
[mkdocs](https://www.mkdocs.org/) and the 
[mkdocstrings](https://mkdocstrings.github.io/usage/) plugin. 
By default, the source code of the base class of your package is used 
to generate docs from your docstrings. If you want to include further modules,
include `*.md` files in the docs folder and 
edit the `mkdocs.yml` file in the project root.

### Auto-Release

Gitlab comes with a PyPi-like Python package repository.
Auto-release to [Gitlab PyPI repository](https://docs.gitlab.com/ce/user/packages/pypi_repository/)
is configured for all tagged pushes to the master/ main branch.

Packages published there can be 
[installed using pip](https://docs.gitlab.com/ee/user/packages/pypi_repository/#install-a-pypi-package):

    pip install --extra-index-url https://<personal_access_token_name>:<personal_access_token>@gitlab.example.com/api/v4/projects/<project_id>/packages/pypi/simple --no-deps <package_name>

For public project, access token is not necessary.

### Gitlab CI

CI Pipeline is configured in `.gitlab-ci.yml`

###  Version Bumping

Bumping the version of your package is done using [bump2version](https://github.com/c4urself/bump2version).
It is configured in the `setup.cfg` to automatically create a commit and a tag for each call of

    bum2version major|minor|patch
    

## Not Exactly What You Want?

Don\'t worry, you have options:

### Similar Cookiecutter Templates

-   [audreyfeldroy/cookiecutter-pypackage/](https://github.com/audreyfeldroy/cookiecutter-pypackage/):
    Audreys template is the base for this one. Geared towards github instead of gitlab, 
    Fetures travis-ci instead of Gitlab-CI and builds docs with sphinx instead of mkdocs.
-   [Nekroze/cookiecutter-pypackage](https://github.com/Nekroze/cookiecutter-pypackage):
    A fork of this with a PyTest test runner, strict flake8 checking
    with Travis/Tox, and some docs and `setup.py` differences.
-   [tony/cookiecutter-pypackage-pythonic](https://github.com/tony/cookiecutter-pypackage-pythonic):
    Fork with py2.7+3.3 optimizations. Flask/Werkzeug-style test runner,
    `_compat` module and module/doc conventions. See `README.rst` or the
    [github comparison
    view](https://github.com/tony/cookiecutter-pypackage-pythonic/compare/audreyr:master...master)
    for exhaustive list of additions and modifications.
-   [ardydedase/cookiecutter-pypackage](https://github.com/ardydedase/cookiecutter-pypackage):
    A fork with separate requirements files rather than a requirements
    list in the `setup.py` file.
-   [lgiordani/cookiecutter-pypackage](https://github.com/lgiordani/cookiecutter-pypackage):
    A fork of Cookiecutter that uses
    [Punch](https://github.com/lgiordani/punch) instead of
    [bump2version](https://github.com/c4urself/bump2version) and with
    separate requirements files.
-   [briggySmalls/cookiecutter-pypackage](https://github.com/briggySmalls/cookiecutter-pypackage):
    A fork using [Poetry](https://python-poetry.org/) for neat package
    management and deployment, with linting, formatting, no makefiles
    and more.
-   [veit/cookiecutter-namespace-template](https://github.com/veit/cookiecutter-namespace-template):
    A cookiecutter template for python modules with a namespace
-   [zillionare/cookiecutter-pypackage](https://zillionare.github.io/cookiecutter-pypackage/):
    A template containing [Poetry](https://python-poetry.org/),
    [Mkdocs](https://pypi.org/project/mkdocs/), Github CI and many more.
    It\'s a template and a package also (can be installed with
    [pip]{.title-ref})
-   Also see the
    [network](https://github.com/audreyr/cookiecutter-pypackage/network)
    and [family
    tree](https://github.com/audreyr/cookiecutter-pypackage/network/members)
    for this repo. (If you find anything that should be listed here,
    please add it and send a pull request!)

### Fork This / Create Your Own

If you have differences in your preferred setup, I encourage you to fork
this to create your own version. Or create your own; it doesn\'t
strictly have to be a fork.

-   Once you have your own version working, add it to the Similar
    Cookiecutter Templates list above with a brief description.
-   It\'s up to you whether or not to rename your fork/own version. Do
    whatever you think sounds good.

### Or Submit a Pull Request

I also accept pull requests on this, if they\'re small, atomic, and if
they make my own packaging experience better.
